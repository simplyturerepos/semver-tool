### Description ###
This script you can use for working with [semantic versioning](https://semver.org/) in PowerShell. Based on this [GitHub Gist](https://gist.github.com/jageall/c5119d5ba26fa33602d1)
### Install ###
```
Invoke-WebRequest -Uri https://bitbucket.org/simplyturerepos/semver-ps/raw/HEAD/semver.ps1 -OutFile semver.ps1
Import-Module .\semver.ps1
```
### Usage ###
```
toSemVer bla-bla-4.6.3

VersionString : bla-bla-4.6.3
Minor         : 6
Pre           : {}
Patch         : 3
Major         : 4
```

```
$versionStr = @("bla-bla-4.6.3","v1.2.3","2.2.3")
$versions = @()
foreach($v in $versionStr) {$versions += toSemVer $v}
$versions = rankedSemVer($versions)
$versions | Sort-Object -Property Rank -Descending | Select-Object -First 1
```